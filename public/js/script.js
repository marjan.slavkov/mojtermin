$(document).ready(function () {
    let timeSlots = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00'];

    $('#date').change(function (e) {

        let date = e.target.value;
        $.ajax({
            url: `/api/date/?date=${date}`,
            type: 'get'
        }).done(function (response) {
            $('#time').empty();
            occupiedTimeSlots = response.data.map(function (i) {
                return i.appointment_time;
            })
            let freeTimeSlots = timeSlots.filter(function(time){
                return !occupiedTimeSlots.includes(time);
            })

            $.each(freeTimeSlots, function (i, time) {
                $('#time').append(`<option value="${time}">${time}</option>`);
            })
        })
    })

})
