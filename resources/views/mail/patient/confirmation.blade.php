@component('mail::message')
    Hi {{ $appointment->name }},

    This is a confirmation mail confirming that you've made an appointment on {{$appointment->appointment_date->format('d-m-Y')}} at {{ $appointment->appointment_date->format('H:i') }}h


    Regards,
    {{ config('app.name') }}
@endcomponent
