@component('mail::message')
Hi {{ $appointment->name }},

Just to remind you that you have an appointment tomorrow at {{ $appointment->appointment_date->format('H:i') }} with our dentist office.


Regards,<br>
{{ config('app.name') }}
@endcomponent
