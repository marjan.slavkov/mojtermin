@include('/partials/header')

<div class="container-fluid background-container">
    <div class="msg-container">
    @if(isset($appointment))
        <div class="row">
            <div class="col-xl-5 offset-xl-4 col-lg-3 offset-lg-7 col-md-3 offset-md-7 mt-5">
                <div class="alert alert-info text-center">Your next appointment is at {{ $appointment->appointment_date->format('H:i \h, \o\n d F Y') }}</div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-xl-4 offset-xl-4 col-lg-4 offset-lg-4 col-md-4 offset-md-4 mt-3">
            @if(session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
                @endif
            @error('bookError')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    </div>

    <div class="row">
        <div class="col-xl-5 offset-xl-6 col-lg-5 offset-lg-6 col-md-5 offset-md-6 form-container d-flex justify-content-center align-items-center">
            <form class="form" method="POST" action="{{ route('booking.store') }}">
                @csrf
                <h1>Book an Appointment</h1>
                <h5>Please select the appointment time best suitable for you and enter your details</h5>
                <div class="row">

                    <div class="col-xl-2 col-lg-2 col-md-2 d-flex flex-column justify-content-start">
                        <div class="form-group">
                            <label for="name" class="labels">Name</label>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 offset-xl-1 offset-lg-1 col-md-9 offset-md-1">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                            @error('name')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-2 d-flex flex-column justify-content-start">
                        <div class="form-group">

                            <label for="phone" class="labels">Phone</label>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 offset-xl-1 offset-lg-1 col-md-9 offset-md-1">
                        <div class="form-group">
                            <input type="tel" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                            @error('phone')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-2 d-flex flex-column justify-content-start">
                        <div class="form-group">
                            <label for="email" class="labels">Email</label>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 offset-xl-1 offset-lg-1 col-md-9 offset-md-1">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                            @error('email')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row d-flex align-items-center">
                    <div class="col-xl-2 col-lg-2 col-md-2 ">
                        <div class="form-group">
                            <label for="appointment" class="labels">Appointment date</label>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 offset-xl-1 offset-lg-1 col-md-9 offset-md-1">
                        <div class="form-group">
                            {{--<label for="date">Date:</label>--}}
                            <input type="date" class="" id="date" name="appointment_date" min="{{ now()->addDay()->format('Y-m-d') }}">
                            @error('appointment_date')
                            <p style="color: red">{{ $message }}</p>
                            @enderror
                            {{--<select class="custom-select mr-sm-2" name="appointment_date" id="inlineFormCustomSelect">
                                <option selected value="">Choose...</option>
                                @foreach($appointments as $time)
                                    <option value="{{ date('Y-m-d H:i', strtotime($time)) }}">{{ date('H:i d-F', strtotime($time)) }}</option>
                                @endforeach
                            </select>
                            @error('appointment_date')
                            <p style="color: red">{{ $message }}</p>
                            @enderror--}}
                        </div>
                    </div>
                </div>
                <div class="row d-flex align-items-center">
                    <div class="col-xl-2 col-lg-2 col-md-2 ">
                        <div class="form-group">
                            <label for="time" class="labels">Appointment time</label>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 offset-xl-1 offset-lg-1 col-md-9 offset-md-1">
                        <div class="form-group time">
                            <select id="time" name="time" size="8">

                            </select>

                        </div>
                        @error('time')
                        <p style="color: red">{{ $message }}</p>
                        @enderror

                    </div>
                    <div class="d-flex justify-content-end btn-book">
                        <button type="submit" class="btn btn-primary">Book in</button>
                    </div>

                </div>

            </form>
        </div>
    </div>
</div>

@include('/partials/footer')
