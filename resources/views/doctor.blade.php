@include('partials.header')

<div class="container mt-5">
    <div class="row text-center text-info">
        <h3>Welcome {{ Auth::user()->name }}</h3>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xl-10 col-lg-10 col-md-10 offset-1">
            <div class="accordion mt-4" id="accordionExample">
                @foreach ($appointments as $date => $appointment)
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                        data-target="#collapse{{ $date }}" aria-expanded="true"
                                        aria-controls="collapseOne">
                                    {{ $date }}
                                </button>
                            </h2>
                        </div>

                        <div id="collapse{{ $date }}" class="collapse" aria-labelledby="headingOne"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                <ul>
                                    @foreach($appointment as $time)
                                        <p class="mb-1 mt-3 appointment-time">Appointment time: {{ $time->appointment_date->format('H:i') }}h</p>
                                    <li>Name: {{ $time->name }}</li>
                                    <li>Phone: {{ $time->phone }}</li>
                                    <li>e-mail: {{ $time->email }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</div>

@include('partials.footer')
