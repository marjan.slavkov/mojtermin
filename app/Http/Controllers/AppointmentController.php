<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Http\Resources\AppointmentsCollection;
use App\Http\Resources\AppointmentsResource;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Events\AppointmentEvent;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::where('appointment_date', '>', now())->get()->sortBy('appointment_date')->groupBy(function ($date) {
            return $date->appointment_date->format('d-F-Y');
        });
        return view('doctor', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$appointmentDates = $this->generateAppointmentDates(7);
        return view('book');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|min:3|max:25',
            'phone' => 'required',
            'email' => 'required|email',
            'appointment_date' => 'required|unique:appointments',
            'time' => 'required',
        ]);

        if (Appointment::where('appointment_date', $request->appointment_date)->exists()) {
            return redirect()->back()->withErrors(['bookError' => 'Бараниот термин е резервиран']);
        }

        $appointment =  Appointment::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'appointment_date' => "$request->appointment_date $request->time",
            'patient_id' => auth()->user()->id,
        ]);

        event(new AppointmentEvent($appointment));

        return redirect()->back()->with(['success' => 'Успешно закажавте термин']);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }

    private function generateAppointmentDates($days)
    {
        //$now = Carbon::now()->hour >= 15 ? Carbon::now()->addDay() : Carbon::now();
        $period = CarbonPeriod::create(Carbon::now()->addDay(), Carbon::now()->addDays($days));
        $dates = [];
        $appointmentDates = [];

        // Iterate over the period
        foreach ($period as $date) {
            if ($date->isWeekday()) {
                $hours = $this->generateDateRange(Carbon::create($date->format('Y-m-d ' . '09:00')), Carbon::create($date->format('Y-m-d ' . '16:00')));
                $dates[$date->format('d-F-Y')] = $hours[$date->format('Y-m-d')];
            }
        }

        foreach ($dates as $key => $val) {
            foreach ($val as $time) {
                array_push($appointmentDates, "{$time} {$key}");
            }
        }
        return $appointmentDates;
    }


    private function generateDateRange(Carbon $startDate, Carbon $endDate, $slotDuration = 60)
    {
        $dates = [];
        $slots = $startDate->diffInMinutes($endDate) / $slotDuration;

        //first unchanged time
        $dates[$startDate->toDateString()][] = $startDate->format('H:i');

        for ($s = 1; $s <= $slots; $s++) {
            $dates[$startDate->toDateString()][] = $startDate->addMinute($slotDuration)->format('H:i');
        }

        return $dates;
    }

    public function getFreeTimeSlots(Request $request)
    {

        return new AppointmentsCollection(Appointment::whereBetween('appointment_date',
            [$request->date, date('Y-m-d H:i', strtotime("$request->date +1 day"))])
            ->get());
    }
}
