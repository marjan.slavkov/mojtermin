<?php

namespace App\Listeners;

use App\Appointment;
use App\Mail\ConfirmationEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\AppointmentEvent;
use Illuminate\Support\Facades\Mail;

class AppointmentEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AppointmentEvent $event)
    {
        Mail::to($event->appointment)->send(New ConfirmationEmail($event->appointment));
    }
}
