<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Mail\PatientReminderMail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class mojtermin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moj-termin:send-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends email reminders to patients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $appointments = Appointment::whereBetween('appointment_date', [Carbon::tomorrow(), Carbon::tomorrow()->addDay()])->get();
        foreach ($appointments as $appointment){
            Mail::to($appointment->email)->send(new PatientReminderMail($appointment));
        }
        echo $appointments->count(). " emails were sent.". PHP_EOL;
    }
}
