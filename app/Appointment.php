<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = ['patient_id', 'name', 'phone', 'email', 'appointment_date'];

    protected $dates = ['appointment_date'];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
