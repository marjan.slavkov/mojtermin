<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');


Route::middleware('auth')->group(function (){
    Route::get('/book', 'AppointmentController@create')->name('booking.index');
    Route::post('/book', 'AppointmentController@store')->name('booking.store');
});


Route::middleware(['doctor_only'])->group(function(){
    Route::get('/dashboard', 'AppointmentController@index')->name('appointments.index');
});


Auth::routes();


